export PARAMS ?= $(filter-out $@,$(MAKECMDGOALS))

env:
	touch .env.local

configure: clean env build
	- docker network create gainy-default

up:
	docker compose -p rentbot up --no-build

upd:
	docker compose -p rentbot up -d

build:
	docker compose -p rentbot build

down:
	docker compose -p rentbot down

clean:
	- docker compose -p rentbot down --rmi local -v --remove-orphans

test:
	docker-compose -f docker-compose.test.yml exec -T test-app pytest

style-check:
	yapf --diff -r rentbot

style-fix:
	yapf -i -r rentbot

%:
	@:
