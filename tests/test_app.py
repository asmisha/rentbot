import datetime

import pytest
from telegram import Update, Message, Chat, User

from rentbot.app import ApplicationHandler
from rentbot.telegram.repository import TelegramRepository


@pytest.mark.asyncio
async def test_app_start(monkeypatch):
    chat_id = 1
    first_name = "first_name"
    username = "username"
    user_created = False

    app_handler = ApplicationHandler()

    tg_repository = TelegramRepository()
    def mock_create_user(_chat_id, _first_name, _username):
        assert _chat_id == chat_id
        assert _first_name == first_name
        assert _username == username
        nonlocal user_created
        user_created = True

    monkeypatch.setattr(tg_repository, "create_user", mock_create_user)
    app_handler._tg_repository = tg_repository

    chat = Chat(chat_id, 'type')
    user = User(0, first_name=first_name, is_bot=False, username=username)
    message = Message(0, datetime.datetime.now(), chat, from_user=user)
    update = Update(0, message=message)
    await app_handler.start(update, None)

    assert user_created