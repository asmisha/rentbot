import pytest
from telegram import Bot

from rentbot import TelegramService
from rentbot.telegram.repository import TelegramRepository
from rentbot.user_link.repository import UserLinkRepository


@pytest.mark.asyncio
async def test(monkeypatch):
    user_id = 1
    chat_id = 2
    tg_token = "tg_token"
    link1 = "link1"
    link2 = "link2"
    sent_links = set()
    saved_links = set()

    async def mock_send_message(self, _chat_id, link):
        assert _chat_id == chat_id
        nonlocal sent_links
        sent_links.add(link)
    monkeypatch.setattr(Bot, "send_message", mock_send_message)

    user_link_repository = UserLinkRepository()
    def mock_add_sent_link(_user_id, link):
        assert _user_id == user_id
        nonlocal saved_links
        saved_links.add(link)
    monkeypatch.setattr(user_link_repository, "add_sent_link", mock_add_sent_link)

    tg_repository = TelegramRepository()
    def mock_get_chat_id_by_user_id(_user_id):
        assert _user_id == user_id
        return chat_id

    monkeypatch.setattr(tg_repository, "get_chat_id_by_user_id", mock_get_chat_id_by_user_id)

    service = TelegramService()
    service._tg_repository = tg_repository
    service._user_link_repository = user_link_repository

    await service.send_links(user_id, [link1, link2])

    assert link1 in sent_links
    assert link2 in sent_links
    assert link1 in saved_links
    assert link2 in saved_links
