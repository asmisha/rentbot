import pytest

from rentbot import ParserService, TelegramService
from rentbot.cronjob import ParseAndSendJob
from rentbot.search.repository import SearchRepository
from rentbot.user_link.repository import UserLinkRepository


@pytest.mark.asyncio
async def test(monkeypatch):
    user_id1 = 1
    user_id2 = 2
    url = "url"
    link1 = "link1"
    link2 = "link2"
    sent_links = False

    search_repository = SearchRepository()
    monkeypatch.setattr(search_repository, "get_url_user_ids", lambda: [(url, [user_id1, user_id2])])

    parser_service = ParserService([])
    def mock_get_latest_links(_url):
        assert _url == url
        return [link1, link2]
    monkeypatch.setattr(parser_service, "get_latest_links", mock_get_latest_links)

    user_link_repository = UserLinkRepository()
    def mock_filter_sent_links(user_id, links):
        assert link1 in links
        assert link2 in links
        assert user_id in [user_id1, user_id2]

        if user_id == user_id1:
            return [link1]
        else:
            return [link2]

    monkeypatch.setattr(user_link_repository, "filter_sent_links", mock_filter_sent_links)

    sender_service = TelegramService()
    async def mock_send_links(user_id, links):
        assert user_id in [user_id1, user_id2]
        if user_id == user_id1:
            assert links == [link1]
        else:
            assert links == [link2]

        nonlocal sent_links
        sent_links = True

    monkeypatch.setattr(sender_service, "send_links", mock_send_links)

    job = ParseAndSendJob()
    job._search_repository = search_repository
    job._parser_service = parser_service
    job._user_link_repository = user_link_repository
    job._sender_service = sender_service

    await job.do()
    assert sent_links