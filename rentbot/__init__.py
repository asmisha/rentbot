import os

import inject

from rentbot.interfaces import SenderService
from rentbot.parsers import OlxParser, OtodomParser
from rentbot.services.parser import ParserService
from rentbot.telegram.service import TelegramService


def inject_config(binder: inject.Binder):
    tg_token = os.getenv("TELEGRAM_TOKEN")
    if tg_token is None:
        raise Exception('TELEGRAM_TOKEN is None')
    binder.bind("TELEGRAM_TOKEN", tg_token)

    binder.bind_to_constructor(SenderService, lambda: TelegramService())
    binder.bind_to_constructor(
        ParserService,
        lambda: ParserService([OtodomParser(), OlxParser()]))


inject.configure(inject_config)
