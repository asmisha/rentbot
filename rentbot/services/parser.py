from rentbot.interfaces import ParserInterface


class ParserService():

    def __init__(self, parsers: list[ParserInterface]):
        self.parsers = parsers

    def get_latest_links(self, url: str) -> list[str]:
        for parser in self.parsers:
            if not parser.supports_url(url):
                continue

            return parser.get_latest_links(url)

        raise Exception(f"No parser found for url {url}")
