import re
import requests
import urllib.parse
from selectolax.parser import HTMLParser
from rentbot.common import get_proxies
from rentbot.interfaces import ParserInterface


class OlxParser(ParserInterface):

    def supports_url(self, url):
        return re.search('^https://www.olx.pl', url) is not None

    def get_latest_links(self, url):
        res = requests.get(url, proxies=get_proxies())

        tree = HTMLParser(res.text)

        hrefs = []
        nodes = tree.css('[data-cy="l-card"]')
        for node in nodes:
            link = node.css_first('a')
            if link is None:
                continue
            hrefs.append(
                urllib.parse.urljoin(url,
                                     node.css_first('a').attributes['href']))

        return hrefs
