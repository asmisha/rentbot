from operator import itemgetter

from rentbot.data_access import BaseRepository


class UserLinkRepository(BaseRepository):

    def filter_sent_links(self, user_id: int, links: list):
        with self.db_conn.cursor() as cursor:
            cursor.execute(
                "select link from user_sent_links where user_id = %(user_id)s and link in %(links)s",
                {
                    "user_id": user_id,
                    "links": tuple(links)
                })
            sent_links = map(itemgetter(0), cursor.fetchall())
        links = list(set(links).difference(set(sent_links)))
        return links

    def add_sent_link(self, user_id, link):
        with self.db_conn.cursor() as cursor:
            cursor.execute(
                "insert into user_sent_links (user_id, link) values (%(user_id)s, %(link)s)",
                {
                    "user_id": user_id,
                    "link": link
                })
