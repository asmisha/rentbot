import os


def get_proxies():
    PROXY_USERNAME = os.getenv('PROXY_USERNAME')
    PROXY_PASSWORD = os.getenv('PROXY_PASSWORD')
    PROXY_HOST = os.getenv('PROXY_HOST')
    PROXY_PORT = os.getenv('PROXY_PORT')

    if not PROXY_HOST or not PROXY_PORT:
        return None

    if PROXY_USERNAME and PROXY_PASSWORD:
        proxy_auth = f"{PROXY_USERNAME}:{PROXY_PASSWORD}@"
    else:
        proxy_auth = ""
    http_proxy = f"http://{proxy_auth}{PROXY_HOST}:{PROXY_PORT}"
    https_proxy = f"https://{proxy_auth}{PROXY_HOST}:{PROXY_PORT}"

    return {
        "http": http_proxy,
        "https": https_proxy,
    }
