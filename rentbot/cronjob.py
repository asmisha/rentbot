import asyncio

import inject as inject

from rentbot.interfaces import SenderService
from rentbot.search.repository import SearchRepository
from rentbot.services.parser import ParserService
from rentbot.user_link.repository import UserLinkRepository


class ParseAndSendJob:
    _sender_service = inject.attr(SenderService)
    _parser_service: ParserService = inject.attr(ParserService)
    _user_link_repository = inject.attr(UserLinkRepository)
    _search_repository = inject.attr(SearchRepository)

    async def do(self):
        urls_processed = 0
        for url, user_ids in self._search_repository.get_url_user_ids():
            links = self._parser_service.get_latest_links(url)
            if not links:
                continue

            for user_id in user_ids:
                new_links = self._user_link_repository.filter_sent_links(
                    user_id, links)
                await self._sender_service.send_links(user_id, new_links)
            urls_processed += 1
        print("Processed %d urls" % (urls_processed))


def cli():
    asyncio.run(ParseAndSendJob().do())
