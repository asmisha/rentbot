import inject
from telegram import Update
from telegram.ext import ApplicationBuilder, CommandHandler, ContextTypes, Application

from rentbot.telegram.repository import TelegramRepository


class ApplicationHandler:
    _tg_repository = inject.attr(TelegramRepository)

    async def start(self, update: Update,
                    context: ContextTypes.DEFAULT_TYPE) -> None:
        self._tg_repository.create_user(update.message.chat_id,
                                        update.effective_user.first_name,
                                        update.effective_user.username)

    def bind(self, app: Application):
        app.add_handler(CommandHandler("start", self.start))


@inject.params(tg_token="TELEGRAM_TOKEN")
def cli(tg_token):
    app = ApplicationBuilder().token(tg_token).build()

    ApplicationHandler().bind(app)

    app.run_polling()
