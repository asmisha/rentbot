from rentbot.data_access import BaseRepository


class SearchRepository(BaseRepository):

    def get_url_user_ids(self):
        with self.db_conn.cursor() as cursor:
            cursor.execute(
                "select url, json_agg(distinct user_id) as user_ids from user_search_urls group by url"
            )
            for row in cursor:
                yield row
