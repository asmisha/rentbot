from abc import ABC, abstractmethod


class SenderService(ABC):

    @abstractmethod
    async def send_links(self, user_id, new_links):
        pass


class ParserInterface(ABC):

    @abstractmethod
    def supports_url(self, url) -> bool:
        pass

    @abstractmethod
    def get_latest_links(self, url) -> list:
        pass
