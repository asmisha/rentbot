import inject

from telegram import Bot

from rentbot import SenderService
from rentbot.telegram.repository import TelegramRepository
from rentbot.user_link.repository import UserLinkRepository


class TelegramService(SenderService):
    _tg_repository = inject.attr(TelegramRepository)
    _user_link_repository = inject.attr(UserLinkRepository)

    @inject.params(tg_token="TELEGRAM_TOKEN")
    def __init__(self, tg_token):
        self._bot = Bot(tg_token)

    async def send_links(self, user_id: int, links: list):
        chat_id = self._tg_repository.get_chat_id_by_user_id(user_id)

        for link in links:
            await self._bot.send_message(chat_id, link)

            self._user_link_repository.add_sent_link(user_id, link)