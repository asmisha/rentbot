import psycopg2

from rentbot.data_access import BaseRepository


class TelegramRepository(BaseRepository):

    def create_user(self, chat_id, first_name, username):
        try:
            with self.db_conn.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO users(first_name) values (%(first_name)s) RETURNING id",
                    {
                        "tg_chat_id": chat_id,
                        "first_name": first_name,
                        "tg_username": username,
                    })
                user_id = cursor.fetchone()[0]

                cursor.execute(
                    "INSERT INTO tg_users(user_id, chat_id, username) values (%(user_id)s, %(chat_id)s, %(username)s)",
                    {
                        "user_id": user_id,
                        "chat_id": chat_id,
                        "username": username,
                    })
            self.db_conn.commit()
        except psycopg2.errors.UniqueViolation:
            self.db_conn.rollback()

    def get_chat_id_by_user_id(self, user_id):
        with self.db_conn.cursor() as cursor:
            cursor.execute(
                "select chat_id from tg_users where user_id = %(user_id)s",
                {"user_id": user_id})
            return cursor.fetchone()[0]
