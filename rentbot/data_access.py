import os

import psycopg2
from psycopg2._psycopg import connection


class BaseRepository:
    _db_conn = None

    @property
    def db_conn(self):
        if not self._db_conn:
            self._db_conn = self._db_connect()

        return self._db_conn

    def _db_connect(self) -> connection:
        HOST = os.getenv("PG_HOST")
        PORT = os.getenv("PG_PORT")
        USERNAME = os.getenv("PG_USERNAME")
        PASSWORD = os.getenv("PG_PASSWORD")
        DB_NAME = os.getenv('PG_DBNAME')
        SCHEMA_NAME = os.getenv('SCHEMA_NAME')

        if not SCHEMA_NAME or not HOST or not PORT or not DB_NAME or not USERNAME or not PASSWORD:
            raise Exception('Missing db connection env variables')

        DB_CONN_STRING = f"postgresql://{USERNAME}:{PASSWORD}@{HOST}:{PORT}/{DB_NAME}?options=-csearch_path%3D{SCHEMA_NAME}"
        return psycopg2.connect(DB_CONN_STRING)