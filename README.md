# RentBot

A small project, which helped me rent my apartment. 

Capabilities:
- Sign up via a telegram bot
- Configure the Search URLs that the bot will monitor for you (not implemented)
- The bot will continuously scrap the urls you configured and send you new offers found on these pages

There are two entry points:
- [Telegram Bot interface](rentbot/app.py) - allows to register
- [Cron job](rentbot/cronjob.py) - parses all the links for all the users and sends new offers to the users via telegram
