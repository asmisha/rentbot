create table if not exists "users"
(
    "id"         serial
        constraint users_pk
            primary key,
    "first_name" text
);

create table if not exists "tg_users"
(
    "user_id"    int primary key,
    "chat_id"    bigint unique,
    "username"   text,
    FOREIGN KEY ("user_id") REFERENCES users ("id") ON UPDATE cascade ON DELETE cascade
);


create table if not exists user_search_urls
(
    "id"      serial
        constraint user_search_urls_pk
            primary key,
    "user_id" integer NOT NULL,
    "url"     varchar NOT NULL,
    FOREIGN KEY ("user_id") REFERENCES "users" ("id") ON UPDATE cascade ON DELETE cascade,
    UNIQUE (user_id, "url")
);

create table if not exists user_sent_links
(
    "id"      serial
        constraint user_sent_links_pk
            primary key,
    "user_id" integer NOT NULL,
    "link"    varchar NOT NULL,
    FOREIGN KEY ("user_id") REFERENCES "users" ("id") ON UPDATE cascade ON DELETE cascade,
    UNIQUE (user_id, "link")
);
