FROM python:3.11-slim

RUN apt update && apt install -y postgresql-client cron
RUN pip install poetry

COPY crontab /var/spool/cron/crontabs/app
RUN crontab /var/spool/cron/crontabs/app
RUN printenv | grep -v "no_proxy" >> /etc/environment

WORKDIR /app
COPY . .
RUN poetry install

ENTRYPOINT ["poetry", "run"]
