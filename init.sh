#!/bin/bash

for i in /usr/lib/postgresql/*/bin; do
  export PATH=$PATH:$i
done

export DB_CONN_STRING="postgresql://$PG_USERNAME:$PG_PASSWORD@$PG_HOST:$PG_PORT/$PG_DBNAME?options=-csearch_path%3D$SCHEMA_NAME"
while ! pg_isready -d "$DB_CONN_STRING"; do sleep 1; done

echo 'Importing seeds'
psql -d "$DB_CONN_STRING" -c "CREATE SCHEMA IF NOT EXISTS $SCHEMA_NAME;"
find seed seed/$ENV -maxdepth 1 -iname '*.sql' | sort | while read -r i; do
  psql -d "$DB_CONN_STRING" -P pager -f "$i"
done
echo "Seeding done"

poetry run "$@"
